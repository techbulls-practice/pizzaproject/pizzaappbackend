package com.demo.PizzaPalace.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {
@ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String,String>> handleMethodArgsNotValidException(MethodArgumentNotValidException ex){
   Map<String,String>errorMap =new HashMap<>();
   ex.getBindingResult().getAllErrors().forEach((error)->{
       String fieldname=((FieldError)error).getField();
       String message = error.getDefaultMessage();
       errorMap.put(fieldname,message);

   });
   return new ResponseEntity<Map<String,String>>(errorMap, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceNotFoundException(ResourceNotFoundException ex) {
        ErrorResponse errorResponse=new ErrorResponse(false, ex.getMessage(), new ErrorResponse.ErrorDetails(404,"Invalid pizza Id !!"));
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }




}
