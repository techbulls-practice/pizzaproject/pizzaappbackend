package com.demo.PizzaPalace.service;

import com.demo.PizzaPalace.entity.Customer;
import com.demo.PizzaPalace.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;
    public void createCustomer(Customer customer) {
        customerRepository.save(customer);

    }
}
