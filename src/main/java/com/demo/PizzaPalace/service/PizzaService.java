package com.demo.PizzaPalace.service;

import com.demo.PizzaPalace.entity.Pizza;
import com.demo.PizzaPalace.exceptions.ResourceNotFoundException;
import com.demo.PizzaPalace.repository.PizzaRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PizzaService {
    @Autowired
    private PizzaRepository pizzaRepository;


    public void createPizza(Pizza pizza) {
        pizzaRepository.save(pizza);

    }

    public Pizza getPizzaDetails(int pizzaId) {
        return pizzaRepository.findById(pizzaId).
                orElseThrow(()-> new ResourceNotFoundException("Failed to get pizza Details as pizza not found with Id :"+pizzaId));

    }

    public List<Pizza> getAllPizzas() {
        return pizzaRepository.findAll();
    }

    public Pizza updatePiazzaDetails(int pizzaId, Pizza pizza) {
        Pizza existingPizza=pizzaRepository.findById(pizza.getPizzaId()).orElseThrow(()-> new ResourceNotFoundException("Failed to update pizza Details as Pizza not found with id " +pizzaId));
        existingPizza.setName(pizza.getName());
        existingPizza.setDescription(pizza.getDescription());
        existingPizza.setType(pizza.getType());
        existingPizza.setImageUrl(pizza.getImageUrl());
        existingPizza.setPriceRegularSize(pizza.getPriceRegularSize());
        existingPizza.setPriceMediumSize(pizza.getPriceMediumSize());
        existingPizza.setPriceLargeSize(pizza.getPriceLargeSize());
      return  pizzaRepository.save(existingPizza);


    }

    public void deletePizza(int pizzaId) {
        pizzaRepository.deleteById(pizzaId);
    }


}
