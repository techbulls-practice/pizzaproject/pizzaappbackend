package com.demo.PizzaPalace.response;

import com.demo.PizzaPalace.entity.Pizza;
import lombok.*;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ApiResponse {
private boolean success;
private String message;
private Object data;


}
