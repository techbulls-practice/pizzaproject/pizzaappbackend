package com.demo.PizzaPalace.repository;


import com.demo.PizzaPalace.entity.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PizzaRepository extends JpaRepository<Pizza,Integer>{

}
