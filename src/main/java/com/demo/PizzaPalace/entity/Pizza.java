package com.demo.PizzaPalace.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pizza {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int pizzaId;
    @NotEmpty
    @Size(min = 4,message = "Pizza name must be of min 4 characters !!")
    private String name;
    @NotEmpty(message = "Pizza description cannot be empty !!")
    private String description;
    @NotEmpty(message = "pizza type cannot be empty !!")
    private String type;
    @NotEmpty(message = "pizza image cannot be empty !!")
    private String imageUrl;
    @NotNull(message = "Price of Regular size pizza cannot be empty !!")
    private Integer priceRegularSize;
    @NotNull(message = "Price of medium size cannot be empty  !!")
    private Integer priceMediumSize;
    @NotNull(message = "Price of large size cannot be empty  !!")
    private Integer priceLargeSize;


}
