package com.demo.PizzaPalace.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.NumberFormat;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int customerId;
    @NotEmpty(message = "firstname of customer cannot empty !!")
    @Size(min=2,message = "firstname must be min of 2 characters !! ")
    private String firstName;
    @NotEmpty(message = "Lastname of customer cannot empty !!")
    private String lastName;
    @NotEmpty(message = "address of customer cannot empty !!")
    private String address;
    @NotEmpty(message = "Phone number cannot be empty !!")
    @Pattern(regexp = "^[0-9]{10}$", message = "Invalid phone number !!")
    private String phoneNumber;
    @Email(message = "Email address is not valid !!")
    private String emailAddress;

}
