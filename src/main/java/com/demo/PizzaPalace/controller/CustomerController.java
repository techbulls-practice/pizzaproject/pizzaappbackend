package com.demo.PizzaPalace.controller;

import com.demo.PizzaPalace.entity.Customer;
import com.demo.PizzaPalace.response.ApiResponse;
import com.demo.PizzaPalace.service.CustomerService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ComponentScan
@Slf4j
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @PostMapping
    public ResponseEntity<?>createCustomer(@Valid @RequestBody Customer customer){
        customerService.createCustomer(customer);
        return  ResponseEntity.ok().body(new ApiResponse(true, "Successfully created customer", customer));
    }
}
