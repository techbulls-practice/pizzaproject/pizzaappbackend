package com.demo.PizzaPalace.controller;

import com.demo.PizzaPalace.entity.Pizza;
import com.demo.PizzaPalace.exceptions.ResourceNotFoundException;
import com.demo.PizzaPalace.repository.PizzaRepository;
import com.demo.PizzaPalace.response.ApiResponse;
import com.demo.PizzaPalace.service.PizzaService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@ComponentScan
@Slf4j
@RequestMapping("/pizzas")
public class PizzaController {
@Autowired
    private PizzaService pizzaService;
@Autowired
private PizzaRepository pizzaRepository;

@PostMapping
    public ResponseEntity<?>createPizza(@Valid @RequestBody Pizza pizza){
        log.info("inside createPizza method");

            pizzaService.createPizza(pizza);

            return ResponseEntity.ok().body(new ApiResponse(true, "Successfully created pizza", pizza));


    }
@GetMapping("/{id}")
    public ResponseEntity<?>getPizzaDetails(@Valid @PathVariable("id") int pizzaId){
     log.info("Inside getPizzaDetails method");

         Pizza pizza1 = pizzaService.getPizzaDetails(pizzaId);
         return ResponseEntity.ok().body(new ApiResponse(true, "Successfully fetched pizza", pizza1));

    }

    @GetMapping
    public ResponseEntity<?>getAllPizzas(){
        log.info("Inside getAllPizzas method");

            List<Pizza> pizzaList = pizzaService.getAllPizzas();
            return ResponseEntity.ok().body(new ApiResponse(true, "Successfully fetched All pizza Details", pizzaList));


    }
@PutMapping("/{id}")
    public ResponseEntity<?>updatePiazzaDetails( @Valid @PathVariable ("id") int pizzaId,@RequestBody Pizza pizza){
        log.info("Inside updatePiazzaDetails method");
        pizza.setPizzaId(pizzaId);

            Pizza existingPizza = pizzaService.updatePiazzaDetails(pizzaId, pizza);
            log.info("Updates Pizza Details successfully");
            return ResponseEntity.ok().body(new ApiResponse(true, "Successfully updated pizza Details", existingPizza));


    }
@DeleteMapping("/{id}")
    public ResponseEntity<?>deletePizza(@PathVariable("id") int pizzaId){
        log.info("Inside deletePizza method");
    Pizza existingPizza=pizzaRepository.findById(pizzaId).orElseThrow(()-> new ResourceNotFoundException("Failed to delete pizza Details as Pizza not found with id " +pizzaId));
       pizzaService.deletePizza(pizzaId);
    log.info("pizza  successfully deleted ");
       return ResponseEntity.ok().build();
    }
}
