package com.demo.PizzaPalace;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;
@Slf4j
@SpringBootApplication
public class PizzaPalaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaPalaceApplication.class, args);
log.info("welcome to Pizzapalace");
	}

}
